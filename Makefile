SHELL = /bin/sh

PREFIX = /usr/local

SRC = src
SCRIPT = bsp

install:
	cp -v $(SRC)/$(SCRIPT) $(DESTDIR)$(PREFIX)/bin
	chmod 0755 $(DESTDIR)$(PREFIX)/bin/$(SCRIPT)

uninstall:
	rm -fv $(DESTDIR)$(PREFIX)/bin/$(SCRIPT)
