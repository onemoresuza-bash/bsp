#!/usr/bin/env bash

set -eo pipefail

readonly _BSP_PROGNAME="${0##*/}"
readonly _BSP_NTFR="${BSP_NOTIFIER}"
#
# This is not a misspelling:
# shellcheck disable=SC2153
#
_BSP_CMDFILE="${BSP_CMDFILE}"

#
# Print a message either to stderr or with the BSP_NOTIFIER program, if set.
#
# Globals:
#   _BSP_PROGNAME
#   _BSP_NTFR
#
# Parameters:
#   1: message.
#
_bsp::ntfr::send_msg() {
  if [[ -n "${_BSP_NTFR}" ]]; then
    ${_BSP_NTFR} "${_BSP_PROGNAME}" "${1}"
  else
    printf "%s: %s\n" "${_BSP_PROGNAME}" "${1}" 2>&1
  fi
}

#
# Print the help message.
#
# Globals:
#   _BSP_PROGNAME
#
# Parameters:
#   NONE
#
_bsp::ntfr::help_msg() {
  printf """

Usage: %s [OPTION]... SELECTION [SUBSIDIARY SELECTIONS...]
A bash selection plumber.

-c CMD,\t\tPass SELECTION's content as CMD's arguments
-i CMD,\t\tPass SELECTION's content as CMD's stdin
-h,\t\tPrint this message to stdout

" "${_BSP_PROGNAME}"
}

#
# Check which display server is running and exit if either Wayland is running or
# X is not.
#
# Globals:
#   DISPLAY
#   WAYLAND_DISPLAY
#
# Parameters:
#   NONE
#
_bsp::display_server::ensure() {
  local display_server="${DISPLAY:+X11}"
  readonly display_server="${display_server:-"${WAYLAND_DISPLAY:+Wayland}"}"

  case "${display_server}" in
    X11) return 0 ;;
    Wayland)
      _bsp::ntfr::send_msg "This script cannot run on Wayland!"
      exit 1
      ;;
    *)
      _bsp::ntfr::send_msg "\"DISPLAY\" variable not set; X is not running"
      exit 1
      ;;
  esac
}

#
# Run the given command with the contents of the given selection either as its
# positional arguments or its stdin
#
# Globals:
#   _BSP_CMDFILE
#
# Parameters:
#   1: either a 'c' for positional arguments or a 'i' for stdin;
#   2-: the selections separated from the command arguements by a '\n'.
#
_bsp::selection::run_cmd() {
  local -r run_type="${1}"
  shift

  local -a selections cmd
  local -r old_ifs="${IFS}"
  IFS=$'\n'

  #
  # We want resplitting of elements:
  # shellcheck disable=SC2068
  #
  for args in ${@}; do
    [ "${#selections[@]}" -eq 0 ] && {
      read -ra selections 0<<<"${args}"
      continue
    }

    [ "${#cmd[@]}" -eq 0 ] && {
      read -ra cmd 0<<<"${args}"
      continue
    }
  done

  IFS="${old_ifs}"

  case "${run_type}" in
    'c')
      # shellcheck disable=SC2068
      for s in ${selections[@]}; do
        xclip -selection "${s}" -o 1>/dev/null 2>&1 || {
          _bsp::ntfr::send_msg "Failed to run \"${cmd[*]}\""
          _bsp::ntfr::send_msg "Selection: \"${s}\""
          continue
        }

        ${cmd[@]} "$(xclip -selection "${s}" -o)"
        break
      done
      ;;
    'i')
      # shellcheck disable=SC2068
      for s in ${selections[@]}; do
        xclip -selection "${s}" -o 1>/dev/null 2>&1 || {
          _bsp::ntfr::send_msg "Failed to run \"${cmd[*]}\""
          _bsp::ntfr::send_msg "Selection: \"${s}\""
          continue
        }

        ${cmd[@]} <<<"$(xclip -selection "${s}" -o)"
        break
      done
      ;;
    'f')
      [[ -r "${_BSP_CMDFILE}" ]] || {
        _bsp::ntfr::send_msg "Couldn't read BSP_CMDFILE: \"${_BSP_CMDFILE}\""
        exit 1
      }
      # shellcheck disable=SC2068
      for s in ${selections[@]}; do
        xclip -selection "${s}" -o 1>/dev/null 2>&1 || {
          _bsp::ntfr::send_msg "Failed to run \"${cmd[*]}\""
          _bsp::ntfr::send_msg "Selection: \"${s}\""
          continue
        }

        bash "${_BSP_CMDFILE}" "$(xclip -selection "${s}" -o)"
        break
      done
      ;;
  esac
}

main() {
  _bsp::display_server::ensure

  local -a cmd_posargs cmd_stdin
  sopts=":hc:i:"
  while getopts "${sopts}" opt; do
    case "${opt}" in
      "c") IFS=" " read -ra cmd_posargs 0<<<"${OPTARG}" ;;
      "i") IFS=" " read -ra cmd_stdin 0<<<"${OPTARG}" ;;
      "h")
        _bsp::ntfr::help_msg
        exit 0
        ;;
      ":")
        _bsp::ntfr::send_msg "Option lacks argument -- \"-${OPTARG}\""
        exit 1
        ;;
      "?")
        _bsp::ntfr::send_msg "Invalid option -- \"-${OPTARG}\""
        exit 1
        ;;
    esac
  done

  [[ "${#cmd_posargs[@]}" -gt 0 && "${#cmd_stdin[@]}" -gt 0 ]] && {
    _bsp::ntfr::send_msg "The options \"-c\" and \"-i\" are incompatible"
    exit 1
  }

  shift $((OPTIND - 1))

  [[ "${#}" -lt 1 ]] && {
    _bsp::ntfr::send_msg "At least one selection must be passed"
    _bsp::ntfr::send_msg "primary, secondary or clipboard"
    exit 1
  }

  local cmd_type="${cmd_posargs[0]:+"posarg"}"
  cmd_type="${cmd_type:-"${cmd_stdin[0]:+"stdin"}"}"
  readonly cmd_type="${cmd_type:-"cmdfile"}"

  case "${cmd_type}" in
    "posarg")
      _bsp::selection::run_cmd "c" "${*}"$'\n'"${cmd_posargs[*]}"$'\n'
      ;;
    "stdin")
      _bsp::selection::run_cmd "i" "${*}"$'\n'"${cmd_stdin[*]}"$'\n'
      ;;
    "cmdfile")
      _bsp::selection::run_cmd "f" "${*}"
      ;;
  esac
}

main "${@}"

# vim: set ft=sh:set expandtab:set shiftwidth=2:set tabstop=2:
