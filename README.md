# bsp

`bsp` is a Bash Selection Plumber.

# Description
`bsp` allows you to quickly send any content in one of the given selections to
the desired program.
